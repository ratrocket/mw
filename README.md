Package mw is my own little collection of http.Handler compliant
middleware.

> **(May 2021)** moved to [md0.org/mw](https://md0.org/mw). Use `import md0.org/mw`

Certainly there are better libraries for doing these things (and I use
some!), so this is somewhere between an exercise and "because I can".
