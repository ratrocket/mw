// Package mw is my own little collection of http.Handler compliant
// middleware.
//
// Certainly there are better libraries for doing these things (and I
// use some!), so this is somewhere between an exercise and "because I
// can".
package mw

import (
	"fmt"
	"log"
	"net/http"
)

func GET(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Resource not found; %s\n", r.URL.Path)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func POST(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Resource not found; %s\n", r.URL.Path)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func Exact(m string, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != m {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Resource not found: %s\n", r.URL.Path)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// TODO log http status (maybe in r.Response.Status ??)
func LogBasic(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s [%s]\n", r.Proto, r.Method, r.URL.Path, r.Header.Get("X-Real-IP"))
		h.ServeHTTP(w, r)
	})
}

// TODO: pulled this out of pwauth.  Think about it, then, if I want it,
// make it so it works with package mw.
//
// This might be dumb.  A middleware to disallow trailing slashes.
// func noTrailingSlashes(h http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		if strings.HasSuffix(r.RequestURI, "/") {
// 			log.Print("noTrailingSlashes: redirecting from: ", r.RequestURI)
// 			http.Redirect(w, r, strings.TrimSuffix(r.RequestURI, "/"), http.StatusMovedPermanently)
// 			return
// 		}
// 		h.ServeHTTP(w, r)
// 	})
// }
